<!doctype html <?php language_attributes(); ?>>
<html lang="en" class="">

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width" , initial-scale="1">

	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css" media="all" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" media="print" />
	<!--[if IE]>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/ie.css" media="screen, projection" />
		<![endif]-->
	<!--[if lt IE 9]>
			<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
		<![endif]-->

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<header role="banner" class="global__header">
		<div class="wrapper">
			<div class="row">
				<div class="brand">
					<span class="brand__name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); // Title it with the blog name ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
					<span class="brand__motto"><?php bloginfo( 'description' ); ?></span>
				</div>

				<a href="" class="logo">
					<span class="logo__img"></span>
				</a>

				<address class="contacts">
					<span class="contact__info">T: (+254) 712 345 678</span>
					<span class="contact__info">E: info@loremhighschool.ac.ke</span>
				</address>
			</div>
		</div>
	</header>
	
	<nav role="navigation" class="global__nav">
		<?php wp_nav_menu( array( 'theme_location' => 'navbar' ) ); ?>
	</nav>
	
	<main class="global__content">