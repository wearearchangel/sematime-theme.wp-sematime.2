<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'NAKED_VERSION', 1.0 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'navbar'		=>	__( 'Navbar Menu', 'naked' ),
		'footer__nav-1'	=>	__( 'Footer Menu 1', 'naked' ),
		'footer__nav-2'	=>	__( 'Footer Menu 2', 'naked' ),
		'footer__nav-3'	=>	__( 'Footer Menu 3', 'naked' ),// Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu, 
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<nav>',	// What to display before each widget
		'after_widget' => '</nav>',	// What to display following each widget
		'before_title' => '<h4 class="nav__header">',	// What to display before each widget's title
		'after_title' => '</h4>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar, 
		// just change the values of id and name to another word/name
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------*/
/* Api manipulation functions
/*-----------------------------------------------------------------------------------------*/
/**
 * Gets api data from Sematime and Cache the data in the wordpress database
 * @param $url
 * @param $transient
 * @return array|mixed|object
 */
function makeHttpGetCallsToSematimeApi($url, $transient){

    $apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

    $args = array(
        'headers' => array( "ApiKey" => $apiKey)
    );

    $response = wp_remote_get( $url, $args );

    if($response){

    $body = wp_remote_retrieve_body( $response );

    set_transient($transient, $body, 60*60 );

    return json_decode($body);
    }
    return false;
}

/**
 * Get the Events in a school for a particular term
 * @param $term
 * @return array|mixed|object
 */
function getEventsForSchoolTerm($term){

    $transient_name = 'SCHOOL_EVENTS';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return  json_decode($transient);
    }

    $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/events/?term='.$term;

    return callAndCache($url, $transient_name);
}


/**
 * Get news for a school
 * @param $rowCount
 * @return array|mixed|object
 */
function getNewsForSchool($rowCount){

    $transient_name = 'SCHOOL_NEWS';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return json_decode($transient);
    }

    $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/news?rowCount='.$rowCount;

    return callAndCache($url, $transient_name);
}


/**
 * Retrieve all the albums for a school
 * @return array|mixed|object
 */
function getAlbumsForSchool(){

    $transient_name = 'PHOTO_ALBUMS';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return json_decode($transient);
    }

    $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/photos';

    return callAndCache($url, $transient_name);
}

/**
 * Retrieve all the photos in a school's album
 * @param $album_id
 * @return array|mixed|object
 */
function getAlbumPhotos($album_id){

    $transient_name = 'PHOTO_IN_ALBUM';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return json_decode($transient);
    }

    $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/photos/'.$album_id;

    return callAndCache($url, $transient_name);
}

/**
 * Retrieve the a school's school fees for a particular unit
 * @param $class_unit
 * @return array|mixed|object
 */
function getSchoolUnitFee($class_unit){

    $transient_name = 'SCHOOL_FEE';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return json_decode($transient);
    }



   $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/fees/?classUnit='.$class_unit;

    return callAndCache($url, $transient_name);
}

/**
 * Retrieve the exam results for a school's single unit
 * @param $class_unit
 * @return array|mixed|object
 */
function getExamResultsForAUnit($class_unit){

    $transient_name = 'EXAM_RESULTS';

    //delete_transient($transient_name);

    if(cacheProcessor($transient_name) === true){

        $transient = get_transient($transient_name);

        return json_decode($transient);
    }


    $url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/exams/?classUnit='.$class_unit;

    return callAndCache($url, $transient_name);
}

/**
 * Handles the process of calling api and caching data from the api
 * @param $url
 * @param $transient
 * @return array|mixed|object
 */
function callAndCache($url, $transient){

    return makeHttpGetCallsToSematimeApi($url, $transient);
}

/**
 * Decides if to call api or to return cached data from the wordpress database
 * @param $transient
 * @return bool
 */
function cacheProcessor($transient){

    $transient_saved = get_transient($transient);

    if(false === $transient_saved){
        return false;
    }

    return true;
}