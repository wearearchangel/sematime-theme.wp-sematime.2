<?php 
/**
 * 	Template Name: Gallery Page
*/
get_header();  ?>

	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title"><?php single_post_title(); ?></h1>
				<p><?php the_content(); ?></p>
			</header>
		</div>
	</header>

	<section class="gallery">
		<div class="wrapper">
			<div class="gallery">
				<?php get_template_part( 'template-parts/content-album' ); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>