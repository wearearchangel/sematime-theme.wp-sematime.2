<?php 
/**
 * 	Template Name: Home Page
*/
get_header();  ?>
	<header class="global__carousel">
		<div class="carousel">
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__1.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__2.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__3.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__4.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__5.jpg" alt="" class="img">
			</figure>
		</div>
	</header>

	<section class="welcome">
		<div class="row">
			<section class="welcome__statement">
				<div class="wrapper">
					<h2 class="title">Welcome to Lorem High School</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam vel mollitia impedit, praesentium vero consequuntur sunt debitis! Dolore, eveniet, consequuntur. Molestias quae nihil, expedita atque modi officia sed quam animi?</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, ea quas numquam voluptate reprehenderit velit, accusantium quae earum nam delectus id quo dolorum commodi architecto molestiae deleniti harum, aliquid obcaecati.</p>
					<address><a rel="author">John Doe - Principal</a></address>
				</div>
			</section>
			<section class="welcome__news">
				<div class="wrapper">
					<h2 class="title">Recent news</h2>
                    <?php get_template_part( 'template-parts/content-news-summary' ); ?>
					<a href="">More news and events</a>
				</div>
			</section>
		</div>
	</section>

	<section class="highlight">
		<p class="highlight__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque sunt iusto maiores! Mollitia qui laudantium laborum explicabo veritatis nisi non, earum est necessitatibus sed iusto vero deserunt, inventore in voluptas!</p>
	</section>

	<section class="gallery">
		<article class="article is-contained is-centered">
			<h2 class="title">An opportunity of a lifetime</h2>
			<p class="article__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora impedit repellendus aliquam amet et, explicabo! Fugiat quasi nesciunt et aperiam exercitationem, doloribus tempora ea aspernatur, veritatis dolore deserunt, iusto facere.</p>
		</article>
		<section class="img__grid">
			<?php get_template_part( 'template-parts/content-album-featured' ); ?>
		</section>
	</section>
<?php get_footer(); ?>