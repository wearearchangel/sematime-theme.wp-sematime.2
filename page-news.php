<?php 
/**
 * 	Template Name: News and Events Page
*/
get_header();  ?>

	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title"><?php single_post_title(); ?></h1>
				<p><?php the_content(); ?></p>
			</header>
		</div>
	</header>
	
	<div class="row">
		<aside class="global__content-nav">
			<div class="wrapper">
				<h4>Events</h4>
				<?php get_template_part( 'template-parts/content-events' ); ?>
			</div>
		</aside>
		<section class="article-list news">
			<div class="wrapper is-contained">
				<?php get_template_part( 'template-parts/content-news' ); ?>
			</div>
		</section>
	</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>