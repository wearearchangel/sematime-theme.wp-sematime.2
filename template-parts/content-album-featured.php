<?php
/**
 * The template part for displaying albums
 */
$json_photo_album = getAlbumsForSchool(); ?>

<?php
foreach($json_photo_album as $albums)
{
?>
<?php
foreach($albums as $album)
 {
 ?>


<figure class="grid-item">
	<img src="<?php echo $album->coverPhotoUrl; ?>" alt="" class="img">
	<figcaption class="img__caption">
		<span class="grid-item__name"><?php echo $album->name; ?></span>
		<span class="grid-item__descr"><?php echo $album->description; ?></span>
		<span class="grid-item__count"><?php echo $album->photosCount; ?></span>
	</figcaption>
</figure>
<?php
 }
}
?>