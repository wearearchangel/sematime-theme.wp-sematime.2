<?php
/**
 * The template part for displaying events
 */
$json_events = getEventsForSchoolTerm(1);?>

<?php
foreach($json_events as $events)
{
	foreach($events as $event)
	{
?>
		<article class="article event">
			<h3 class="article__title"><?php echo $event->name ?></h3>
			<div class="article__date">
				<time class="article__start-date">
					<?php echo $event->startsOn = the_time('l, F jS, Y')?>
				</time>
				<time class="article__start-date">
					<?php echo $event->endsOn = the_time('l, F jS, Y') ?>
				</time>
			</div>
			<p class="article__intro">
				<?php echo $event->description?>
			</p>
		</article>
    <?php
    }
}
?>