<?php
/**
 * The template part for displaying news
 */


$json_articles = getNewsForSchool(3) ;?>

<?php
    foreach($json_articles as $articles)
    foreach($articles as $article)
    {
?>
    <article class="article news__item">
        <h3 class="article__title">
            <a href="" title="Article">
                <?php echo $article->title; ?>
            </a>
        </h3>
        <div class="article__meta">
            <time class="article__date">
                <?php the_time('m/d/Y'); ?>
            </time>
            <span class="article__comments">
            <?php echo $article->commentsCount; ?>
        </span>
            <span class="article__author">
            <?php echo $article->author; ?>
        </span>
        </div>
        <p class="article__intro">
            <?php echo $article->excerpt; ?>
        </p>
    </article>
<?php

   }
?>