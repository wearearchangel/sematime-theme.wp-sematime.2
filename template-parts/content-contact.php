<?php
/**
 * The template used for displaying contacts
 */
?>

<article class="contact">
	<section class="contact__map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15956.360490919842!2d37.023930150000005!3d-1.0946928999999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f461f24be33fd%3A0x69bff890ebf613a1!2sPhilo+One+Apartment%2C+Juja!5e0!3m2!1sen!2ske!4v1455069712879" width="100%" height="" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>
	<section class="contact__content">
		<?php the_content(); ?>
	</section>
</article>