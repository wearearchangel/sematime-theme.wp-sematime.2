<?php
/**
 * The template used for displaying page content
 */
?>
	
	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail('post-thumbnail', array('class'	=> "global__cover"));
				}?>
				<h1 class="title"><?php the_title(); ?></h1>
				<p><?php the_excerpt(); ?></p>
			</header>
		</div>
	</header>
	
	<div class="wrapper">
		<section class="global__content-body">
			<div class="wrapper is-contained">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
						the_content();

						wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
							'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
							'separator'   => '<span class="screen-reader-text">, </span>',
						) );
					?>
				</article><!-- #post-## -->
			</div><!-- .entry-content -->

			<div class="row article__thumb-grid">
				<?php
					$parent = $post->ID;
					query_posts('posts_per_page=5&post_type=page&post_parent='.$parent);

					while (have_posts()) : the_post();		
					// print_r($post);	
				?>
					<a href="<?php the_permalink() ?>" class="article__thumb-grid__item">
						<img class="thumb" src="<?php echo esc_url( get_post_meta( get_the_ID(), 'thumb', true ) ); ?>" alt="<?php the_title_attribute(); ?>" />
						<h4><?php echo $post->post_title; ?></h4>
					</a>
				<?php endwhile; ?>			
			</div>
		</section>
	</div>