<?php
/**
 * The template used for displaying page content
 */
?>

<div class="wrapper">
	<aside class="global__content-nav">
		<nav class="nav">
		<?php 
			$parent = wp_get_post_parent_id( $post_ID );
			if(intval($post->post_parent)>0)
			{
				$post = get_post($post->post_parent);
			}
			echo '<h4 class="nav__header">'.get_the_title($post->ID).'</h4>';
			
			$children = wp_list_pages('title_li=&child_of='.$parent.'&echo=0');
			if ($children) { 
				echo '<ul class="nav__item-container">'.$children.'</ul>';
			}
		?>
		</nav>
	</aside>
	<section class="global__content-body">
	<?php
				if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-department', 'department' );
					endwhile;
				else :
					get_template_part( 'template-parts/content-department', 'none' );
				endif;
			?>
		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?>
		</div><!-- .entry-content -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
				$parent = $post->ID;
				query_posts('posts_per_page=5&post_type=page&post_parent='.$parent);

				while (have_posts()) : the_post();		
				// print_r($post);	
			?>
				<a href="<?php the_permalink() ?>" rel="bookmark">
					<img class="thumb" src="<?php echo esc_url( get_post_meta( get_the_ID(), 'thumb', true ) ); ?>" alt="<?php the_title_attribute(); ?>" />
					<h4><?php echo $post->post_title; ?></h4>
				</a>
			<?php endwhile; ?>
		</article><!-- #post-## -->
	</section>
</div>