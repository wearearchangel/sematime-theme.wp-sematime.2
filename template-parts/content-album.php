<?php
/**
 * The template part for displaying albums
 */
$json_photo_album = getAlbumsForSchool(); ?>

<?php
foreach($json_photo_album as $albums)
{
?>
<?php
foreach($albums as $album)
 {
 ?>
<a href="" class="album__container">
	<figure class="album">
		<div class="album__count"><?php echo $album->photosCount; ?></div>
		<div class="album__cover">
			<img src="<?php echo $album->coverPhotoUrl; ?>" alt="" class="img">
		</div>
		<figcaption class="album__caption">
			<span class="album__name"><?php echo $album->name; ?></span>
			<span class="album__descr"><?php echo $album->description; ?></span>
		</figcaption>
	</figure>
</a>
<?php
 }
}
?>