<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine 
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); ?>

	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title"><?php single_post_title(); ?></h1>
				<p><?php the_excerpt(); ?></p>
			</header>
		</div>
	</header>
	
	<div class="wrapper">
		<section class="news is-contained">
			<?php if ( have_posts() ) : ?>
				<?php if ( is_home() && ! is_front_page() ) : ?>
					<?php single_post_title(); ?>
				<?php endif; ?>

				<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-news' );
					endwhile;

				else :
					get_template_part( 'template-parts/content-news', 'none' );
				endif;
			?>
		</section>
	</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>