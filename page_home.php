<?php 
/**
 * 	Template Name: Home Page
*/
get_header();  ?>
	<header class="global__carousel">
		<div class="carousel">
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__1.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__2.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__3.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__4.jpg" alt="" class="img">
			</figure>
			<figure class="carousel__img">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel-img__5.jpg" alt="" class="img">
			</figure>
		</div>
	</header>

	<section class="welcome">
		<div class="row">
			<section class="welcome__statement">
				<div class="wrapper">
					<h2 class="title">Welcome to Lorem High School</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam vel mollitia impedit, praesentium vero consequuntur sunt debitis! Dolore, eveniet, consequuntur. Molestias quae nihil, expedita atque modi officia sed quam animi?</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, ea quas numquam voluptate reprehenderit velit, accusantium quae earum nam delectus id quo dolorum commodi architecto molestiae deleniti harum, aliquid obcaecati.</p>
					<address><a rel="author">John Doe - Principal</a></address>
				</div>
			</section>
			<section class="welcome__news">
				<div class="wrapper">
					<h2 class="title">Latest News and Events</h2>
					<?php if ( have_posts() ) : ?>
						<?php 
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/content-news', get_post_format() );
							endwhile;
						?>
					<?php else : ?>
						<article class="article error">
							<h1 class="title">Nothing has been posted like that yet</h1>
						</article>
					<?php endif; ?>						
					<a href="">More news and events</a>
				</div>
			</section>
		</div>
	</section>

	<section class="highlight">
		<p class="highlight__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque sunt iusto maiores! Mollitia qui laudantium laborum explicabo veritatis nisi non, earum est necessitatibus sed iusto vero deserunt, inventore in voluptas!</p>
	</section>

	<section class="gallery">
		<article class="article is-contained is-centered">
			<h2 class="title">An opportunity of a lifetime</h2>
			<p class="article__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora impedit repellendus aliquam amet et, explicabo! Fugiat quasi nesciunt et aperiam exercitationem, doloribus tempora ea aspernatur, veritatis dolore deserunt, iusto facere.</p>
		</article>
		<section class="img__grid">
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__1.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 1</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__2.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 2</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__3.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 3</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__4.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 4</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__5.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 5</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__6.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 6</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__7.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 7</figcaption>
			</figure>
			<figure class="grid-item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/experience__8.jpg" alt="" class="img">
				<figcaption class="img__caption">Experience 8</figcaption>
			</figure>
		</section>
	</section>
<?php get_footer(); ?>