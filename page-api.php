<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php

/*$event_url = ' http://sematimeplusapi.elasticbeanstalk.com/v1/events/?term=1';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $event_url, $args );

$body = wp_remote_retrieve_body( $response );



$json_events = json_decode($body);
*/

$json_events = getEventsForSchoolTerm(1);

if($json_events == false){

   echo "<h2>Events could not be loaded</h2>";

}
?>

<h2 style="margin-left: 40%;">Events</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Event</td>
            <td>Description</td>
            <td>Start</td>
            <td>End</td>
        </tr>
        </thead>

        <?php
        foreach($json_events as $events)
        {
            ?>


            <?php
            foreach($events as $event)
            {
                ?>
                <tr>
                    <td><?php echo $event->name ?></td>
                    <td><?php echo $event->description?></td>
                    <td><?php echo $event->startsOn = the_time('l, F jS, Y')?></td>
                    <td><?php echo $event->endsOn = the_time('l, F jS, Y') ?></td>
                </tr>
            <?php
            }

        }
        ?>
    </table>

</div>

<?php

/*$news_url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/news';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $news_url, $args );

$body = wp_remote_retrieve_body( $response );

$json_articles = json_decode($body);
*/

$json_articles = getNewsForSchool(3);

if($json_articles == false){

    echo "<h2>Articles could not be loaded</h2>";

}

?>
<h2 style="margin-left: 40%;">News</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Article</td>
            <td>Excerpt</td>
            <td>Comments</td>
            <td>Author</td>
        </tr>
        </thead>


        <?php
        foreach($json_articles as $articles)
        {
            ?>


            <?php
            foreach($articles as $article)
            {
                ?>
                <tr>
                    <td><?php echo $article->title; ?></td>
                    <td><?php echo $article->excerpt; ?></td>
                    <td><?php echo $article->commentsCount; ?></td>
                    <td><?php echo $article->author; ?></td>
                </tr>
            <?php
            }

        }
        ?>
    </table>
</div>

<?php

/*$photos_url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/photos';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $photos_url, $args );

$body = wp_remote_retrieve_body( $response );

$json_photo_album = json_decode($body);
*/
$json_photo_album = getAlbumsForSchool();

if($json_photo_album == false){

    echo "<h2>School Albums could not be loaded</h2>";

}

?>
<h2 style="margin-left: 40%;">Albums</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Album Name</td>
            <td>Album Description</td>
            <td>Photos Count</td>
            <td>Cover Photo Url</td>
        </tr>
        </thead>

        <?php
        foreach($json_photo_album as $albums)
        {
            ?>


            <?php
            foreach($albums as $album)
            {
                ?>
                <tr>
                    <td><?php echo $album->name; ?></td>
                    <td><?php echo $album->description; ?></td>
                    <td><?php echo $album->photosCount; ?></td>
                    <td><?php echo $album->coverPhotoUrl; ?></td>
                </tr>
            <?php
            }

        }
        ?>
    </table>
</div>

<?php

/*$album_photos_url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/photos/1452772009740';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $album_photos_url, $args );

$body = wp_remote_retrieve_body( $response );

$json_album_photos = json_decode($body);
*/

$json_album_photos = getAlbumPhotos(1452772009740);

if($json_album_photos == false){

    echo "<h2>Photos could not be loaded</h2>";
}

?>
<h2 style="margin-left: 40%;">Album 1 Photos</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Photo Url</td>
        </tr>
        </thead>

        <?php
        foreach($json_album_photos as $photos)
        {
            ?>


            <?php
            foreach($photos as $photo)
            {
                ?>
                <tr>
                    <td><?php echo $photo; ?></td>
                </tr>
            <?php
            }

        }
        ?>
    </table>
</div>
<?php

/*$unit_fee_url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/fees/?classUnit=1';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $unit_fee_url, $args );

$body = wp_remote_retrieve_body( $response );

$json_unit_fee = json_decode($body);
*/

$json_unit_fee = getSchoolUnitFee(1);

if($json_unit_fee == false){

    echo "<h2>School Fees could not be loaded</h2>";

}

?>
<h2 style="margin-left: 40%;">Form one schoolfees breakdown</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Votehead</td>
            <td>Term One Amount</td>
            <td>Term Two Amount</td>
            <td>Term Three Amount</td>
        </tr>
        </thead>

        <?php
        foreach($json_unit_fee as $fees)
        {
            ?>


            <?php
            foreach($fees as $fee)
            {
                ?>
                <tr>
                    <td><?php echo $fee->votehead; ?></td>
                    <td><?php echo $fee->termOneAmount; ?></td>
                    <td><?php echo $fee->termTwoAmount; ?></td>
                    <td><?php echo $fee->termThreeAmount; ?></td>
                </tr>
            <?php
            }

        }
        ?>
    </table>
    <table>
        <thead>

        <td>Term one Total</td>

        <td>Term two Total</td>

        <td>Term three Total</td>

        </thead>

        <td><?php echo $json_unit_fee->termOneTotal; ?></td>

        <td><?php echo $json_unit_fee->termTwoTotal; ?></td>
        <td><?php echo $json_unit_fee->termThreeTotal; ?></td>

    </table>
</div>

<?php

/*$exam_results_url = 'http://sematimeplusapi.elasticbeanstalk.com/v1/exams/?classUnit=1';

$apiKey = "f9a0b34ec69d402985eaf5af9e8050f2";

$args = array(
    'headers' => array( "ApiKey" => $apiKey)
);

$response = wp_remote_get( $exam_results_url, $args );

$body = wp_remote_retrieve_body( $response );

$json_results = json_decode($body);
*/

$json_results = getExamResultsForAUnit(1);

if($json_results == false){

    echo "<h2>Results could not be loaded</h2>";

}

$array = (array) $json_results;

$array_two = (array) $array['examResults'];

$array_three = (array)$array_two['metadata'];

$top_ten_object = $array_two['topTen'];

$bottom_ten_object = $array_two['bottomTen'];

$top_ten_array= (array) $top_ten_object;

$bottom_ten_array =(array) $bottom_ten_object;

?>
<h2 style="margin-left: 40%;">Form one Exam results breakdown</h2>
<div>

    <table style="border: 1px solid #000000;">
        <thead>
        <tr>
            <td>Exam Name</td>
            <td>Class Size</td>
            <td>Class Average</td>
            <td>Highest Score</td>
            <td>Lowest Score</td>
            <td>Term</td>
            <td>Year</td>
        </tr>
        </thead>

        <tr>
            <td>
                <?php echo $array_three['examName']; ?>
            </td>
            <td>
                <?php echo $array_three['classSize']; ?>
            </td>
            <td>
                <?php echo $array_three['classAverage']; ?>
            </td>
            <td>
                <?php echo $array_three['highestScore']; ?>
            </td>
            <td>
                <?php echo $array_three['lowestScore']; ?>
            </td>
            <td>
                <?php echo $array_three['term']; ?>
            </td>
            <td>
                <?php echo $array_three['year']; ?>
            </td>
        </tr>
    </table>


    <h2 style="margin-left: 40%;">Top 10 Form 1 results</h2>
    <table>
        <thead>
        <tr>
            <td>Position</td>
            <td>Student Name</td>
            <td>Score</td>
        </tr>
        </thead>

        <?php

        foreach($top_ten_array as $top_ten){

            $top_ten = (array)$top_ten;


            ?>
            <tr>
                <td><?php echo $top_ten['position'] ?></td>
                <td><?php echo $top_ten['studentName'] ?></td>
                <td><?php echo $top_ten['score'] ?></td>
            </tr>
        <?php

        }
        ?>

    </table>

    <h2 style="margin-left: 40%;">Bottom 10 Form 1 results</h2>
    <table>
        <thead>
        <tr>
            <td>Position</td>
            <td>Student Name</td>
            <td>Score</td>
        </tr>
        </thead>

        <?php

        foreach($bottom_ten_array as $bottom_ten){

            $bottom_ten = (array)$bottom_ten;


            ?>
            <tr>
                <td><?php echo $bottom_ten['position'] ?></td>
                <td><?php echo $bottom_ten['studentName'] ?></td>
                <td><?php echo $bottom_ten['score'] ?></td>
            </tr>
        <?php

        }
        ?>

    </table>








