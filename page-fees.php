<?php 
/**
 * 	Template Name: Fees Page
*/
get_header();  ?>

	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title"><?php single_post_title(); ?></h1>
				<p>Test Fees.</p>
			</header>
		</div>
	</header>
	
	<div class="wrapper">
		<section class="article-list news is-contained">
			<?php get_template_part( 'template-parts/content-fees' ); ?>
		</section>
	</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>