<?php 
/**
 * 	Template Name: Contacts Page
*/
get_header();  ?>

	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title"><?php single_post_title(); ?></h1>
				<p><?php the_content(); ?></p>
			</header>
		</div>
	</header>
	
	<?php get_template_part( 'template-parts/content', 'contact' ); ?>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>