/*jslint browser: true*/
/*global $, jQuery, alert, console, autosize*/

$(document).ready(function ($) {
	"use strict";

	$('.carousel').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: true,
		infinite: true,
		fade: true,
		adaptiveHeight: false
	});
});