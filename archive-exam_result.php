<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>
	<header class="global__content-header">
		<div class="wrapper">
			<header class="page-header is-contained is-centered">
				<h1 class="title">Exam Results</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur dolorum expedita sint quidem accusamus in, labore! Praesentium, impedit ad doloribus officiis magni cupiditate, quos quia numquam excepturi cum vitae?</p>
			</header>
		</div>
	</header>
	
	<div class="wrapper">
		<aside class="global__content-nav">
			<nav class="nav">
				<a href="" class="nav__link">Curriculum</a>
				<a href="" class="nav__link">Facilities/ Amenities</a>
				<a href="" class="nav__link">Departments</a>
				<a href="" class="nav__link">Admission</a>
				<a href="" class="nav__link">Co-curicular</a>
			</nav>
			<?php get_sidebar(); ?>
		</aside>
		<section class="exam-results">
			<?php
			if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content-exam_result', get_post_format() );
				endwhile;
			else :
				get_template_part( 'template-parts/content-exam_result', 'none' );
			endif;
			?>
		</section>
	</div>

<?php get_footer(); ?>