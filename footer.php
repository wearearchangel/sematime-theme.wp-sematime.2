	</main><!-- / end page container, begun in the header -->
	
	<footer class="global__footer" role="contentinfo">
		<div class="wrapper">
			<section class="footer__level-1">
				<div class="row">
					<?php wp_nav_menu( array( 'theme_location' => 'footer__nav-1' ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'footer__nav-2' ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'footer__nav-3' ) ); ?>
				</div>
			</section>
			<section class="footer__level-2">
				<div class="row">
					<nav class="nav nav-inline">
						<span class="nav__item">&copy; 2014 Lorem High School</span>
						<span class="nav__item">P. O. Box 34173, Nairobi-Kenya</span>
					</nav>
				</div>
				<div class="row">
					<nav class="nav nav-inline">
						Built on <a href="http://wordpress.org" rel="generator">Wordpress</a> and <a href="http://wearearchangel.com" rel="designer">Handcrafted in Kenya</a>
					</nav>
				</div>
			</section>
		</div>
	</footer>

	<?php wp_footer(); ?>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/lib/jquery.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/lib/slick.min.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/app/global.js"></script>
	</body>
</html>
