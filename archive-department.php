<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<header class="global__content-header">
			<div class="wrapper">
				<header class="page-header is-contained is-centered">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail('post-thumbnail', array('class'	=> "global__cover"));
					}?>
					<h1 class="title">
						<?php 
							$post_type = get_post_type_object( get_post_type($post) );
							echo $post_type->label ;	
						?>
					</h1>
					<p><?php the_content(); ?></p>
				</header>
			</div>
		</header>

	
		<div class="wrapper">
			<section class="article-grid is-contained">
				<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
				?>
				<div class="article-grid__item">
					<?php get_template_part( 'template-parts/content', 'department' );	 ?>
				</div>
				<?php
				// End the loop.
				endwhile;
				?>		
			</section>
		</div>
		
		<?php
		// Previous/next page navigation.
		the_posts_pagination( array(
			'prev_text'          => __( 'Previous page', 'twentysixteen' ),
			'next_text'          => __( 'Next page', 'twentysixteen' ),
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
		) );

	// If no content, include the "No posts found" template.
	else :
		get_template_part( 'template-parts/content', 'none' );

	endif;
	?>
	
<?php get_footer(); ?>